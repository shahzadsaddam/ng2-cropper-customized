"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var imageCropperComponent_1 = require("./imageCropperComponent");
var ImageCropperModule = (function () {
    function ImageCropperModule() {
    }
    return ImageCropperModule;
}());
ImageCropperModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [common_1.CommonModule],
                declarations: [imageCropperComponent_1.ImageCropperComponent],
                exports: [imageCropperComponent_1.ImageCropperComponent]
            },] },
];
/** @nocollapse */
ImageCropperModule.ctorParameters = function () { return []; };
exports.ImageCropperModule = ImageCropperModule;
//# sourceMappingURL=imageCropperModule.js.map